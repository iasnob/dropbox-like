<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ES">
<head>
	<meta charset="UTF-8">
	<title>PHP Cajón</title>
</head>
<body style="background-color:grey">

<?php

$errors = [];


$_SESSION['login'] = true;
$_SESSION['frontPage'] = false;
$_SESSION['editPage'] = false;
$_SESSION['editingPage'] = false;

if (!empty($_SESSION['user'])){
    setPage(false, true, false, false, false);
}

function setPage($login, $front, $edit, $editing, $mover){
    $_SESSION['login'] = $login;
    $_SESSION['frontPage'] = $front;
    $_SESSION['editPage'] = $edit;
    $_SESSION['editingPage'] = $editing;
    $_SESSION['mover'] = $mover;
}



if (empty($_SESSION['workingDirectory'])){
            $_SESSION['workingDirectory'] = '';
}
if (empty($_SESSION['actualFolder'])){
        $_SESSION['actualFolder'] = '';
}


function printFoldersTree($dir, $dirToMove){
    $files = array_diff(scandir($dir), array('.', '..'));;
    $tree = '';

    foreach ($files as $file) {
        if (is_dir("$dir/$file") && "$dir/$file" != $dirToMove){
            $tree .= <<<TREE
            <form action='' method='post'>
            <input type='submit' name='pathToMove' value='{$dir}/{$file}'/>
            <input type='hidden' name='dirToMove' value='{$dirToMove}'/>
            </form>
            <br>
            TREE;
            $tree .= printFoldersTree("$dir/$file", $dirToMove);
        }
    }
    return $tree;
}


function scanDirectory(){
    $files = array_diff(scandir($_SESSION['root'] . '/' . $_SESSION['workingDirectory']), array('.', '..'));
    $directoriesArray = [];
    $filesArray = [];
    foreach($files as $file){
        if(is_dir($_SESSION['root'] . '/' . $_SESSION['workingDirectory'] . '/' . $file)){
            $directoriesArray[]  = $file;
        }else{
            $filesArray[]    = $file;
        }
     }

     $_SESSION['scanedDir'] = array_merge($directoriesArray, $filesArray);
}

function delTree($dir) {
    $files = array_diff(scandir($dir), array('.','..'));
     foreach ($files as $file) {
       (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
     }
     return rmdir($dir);
}

//validación de nombre de carpeta o fichero
function checkName($fileOrFolder){
    if (strrpos($fileOrFolder, '?') !== false || strrpos($fileOrFolder, '"') !== false || strrpos($fileOrFolder, '|') !== false || 
        strrpos($fileOrFolder, '*') !== false || strrpos($fileOrFolder, '<') !== false || strrpos($fileOrFolder, '>') !== false || 
        strrpos($fileOrFolder, ':') !== false || strrpos($fileOrFolder, '\\') !== false || strrpos($fileOrFolder, '/') !== false){
            return false;
    }else{
        return true;
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST"){
    
    if(!empty($_POST['crearCarpeta'])){
        if (!empty($_POST['carpeta'])){
            if (checkName($_POST['carpeta'])){
                if (file_exists($_SESSION['root'] . '/' . $_SESSION['workingDirectory'] . '/' . $_POST['carpeta'])){
                    $errors[] = "El nombre de la carpeta ya existe.";
                }else{
                    mkdir($_SESSION['root'] . '/' . $_SESSION['workingDirectory'] . '/' . $_POST['carpeta']);
                    scanDirectory();    
                }
            }else{
                $errors[] = "Los nombres de carpeta o archivo no pueden contener ninguno".PHP_EOL."de los siguientes caracteres: /\\:*?\"<>|";
            }
        }else{
            $errors[] = "Escribe un nombre de carpeta.";
        }
        setPage(false, true, false, false, false);
    }

    if(!empty($_POST['subirFichero'])){
        if (count($_FILES['fichero']['name']) > 0){
            if (!in_array(1, $_FILES['fichero']['error'])){
                for ($i=0; $i < count($_FILES['fichero']['name']); $i++){
                    $f1_name = $_FILES['fichero']['name'][$i];
                    $temp_name = $_FILES['fichero']['tmp_name'][$i];
                    move_uploaded_file($temp_name, $_SESSION['root'] . '/' . $_SESSION['workingDirectory'] . '/' . $f1_name);
                }
                scanDirectory();
            }else{
                $errors[] = "Hubo algún error al subir los ficheros.";
            }
        }else{
            $errors[] = "Selecciona algún fichero para subir.";
        }
        setPage(false, true, false, false, false);
    }

    if (!empty($_POST['entrar'])){
        $_SESSION['workingDirectory'] .= '/'.$_POST['dir'];
        scanDirectory();
        $_SESSION['actualFolder'] = $_POST['dir'];
        setPage(false, true, false, false, false);
    }

    if (!empty($_POST['atras'])){
        $_SESSION['workingDirectory'] = str_replace('/'.$_SESSION['actualFolder'], '', $_SESSION['workingDirectory']);
        scanDirectory();
        $arrayExploded = explode('/',$_SESSION['workingDirectory']);
        $_SESSION['actualFolder'] = end($arrayExploded);
        setPage(false, true, false, false, false);
    }

    if (!empty($_POST['eliminar'])){
        delTree($_SESSION['root'] . '/' . $_SESSION['workingDirectory'] . '/' . $_POST['dir']);
        scanDirectory();
        setPage(false, true, false, false, false);
    }

    if (!empty($_POST['editar'])){
        setPage(false, false, true, false, false);
    }

    if (!empty($_POST['cancelar']) || !empty($_POST['volver'])){
        setPage(false, true, false, false, false);
    }

    if (!empty($_POST['borrar'])){
        unlink($_SESSION['root'] . '/' . $_SESSION['workingDirectory'] . '/' . $_POST['file']);
        scanDirectory();
        setPage(false, true, false, false, false);
    }

    if (!empty($_POST['renombrar'])){
        setPage(false, false, false, true, false);
    }

    if (!empty($_POST['cambiar'])){
        if (checkName($_POST['newName'])){
            //setPage(false, true, false, false, false);
            rename($_SESSION['root'] . '/' . $_SESSION['workingDirectory'] . '/' . $_POST['oldName'], $_SESSION['root'] . '/' . $_SESSION['workingDirectory'] . '/' . $_POST['newName']);
            scanDirectory();
        }else{
            $errors[] = "Los nombres de carpeta o archivo no pueden contener ninguno".PHP_EOL."de los siguientes caracteres: /\\:*?\"<>|";
            setPage(false, false, false, true, false);
            $_POST['file'] = $_POST['oldName'];
        }
    }

    if (!empty($_POST['pathToMove'])){

        echo "<br><br>";
        var_dump($_POST['dirToMove']);
        echo "<br><br>";
        var_dump($_POST['pathToMove']);
        echo "<br><br>";
        //rename($_POST['dirToMove'], $_POST['pathToMove']);


        setPage(false, false, false, false, true);
    }
    
    if (!empty($_POST['login'])){

        $username = $_POST['userName'];
        $password = $_POST['password'];

        if (file_exists("credentials.txt")){
            $credentials = file_get_contents("credentials.txt");
            $credentials = unserialize($credentials);
        }else{
            $credentials = [];
        }

        if (array_key_exists($username, $credentials)){
            if (password_verify($password, $credentials[$username])){
                $_SESSION['user'] = $username;
                setPage(false, true, false, false, false);
                $_SESSION['root'] = "cajoncito";
                $_SESSION['workingDirectory'] = $username;
                $_SESSION['actualFolder'] = $username;
                scanDirectory();
            }else{
                $errors[] = "Contraseña incorrecta.";
            }
        }else{
            $errors[] = "Usuaria incorrecta.";
        }
        
    }

    if (!empty($_POST['logout'])){
        setPage(true, false, false, false, false);
        $_SESSION['actualFolder'] = '';
        $_SESSION['workingDirectory'] = '';
        $_SESSION['user'] = '';
    }

    if (!empty($_POST['register'])){

        $username = $_POST['userName'];
        $password = $_POST['password'];

        if (file_exists("credentials.txt")){
            $credentials = file_get_contents("credentials.txt");
            $credentials = unserialize($credentials);
        }else{
            $credentials = [];
        }

        if (array_key_exists($username, $credentials)){
            $errors[] = "Nombre de usuaria ya en uso. Escoge otro.";
        }else{
            $password = password_hash($password, PASSWORD_DEFAULT);
            $credentials[$username] = $password;
            $credentials = serialize($credentials);
            file_put_contents("credentials.txt", $credentials);

            mkdir($_SESSION['root'] . '/' . $username);
            $_SESSION['root'] = "cajoncito";
            $_SESSION['workingDirectory'] = $username;
            $_SESSION['actualFolder'] = $username;
            scanDirectory();
            $_SESSION['user'] = $username;

            setPage(false, true, false, false, false);
        }
    }
}


if ($errors){
    echo "<pre>";
    foreach($errors as $error){
        echo "<br><strong style='color:#ad2b0f;font-size:25px'>".$error."</strong><br>";
    }
    echo "</pre>";
}

echo "working directory: " . $_SESSION['workingDirectory'];
if ($_SESSION['frontPage'] == true){ ?>

    <?php //echo "working directory: " . $_SESSION['workingDirectory']; ?>
    <br><br>
    <?php
    //mostrar ficheros y directorios, cada cual con su form
    if ($_SESSION['workingDirectory'] != $_SESSION['user']){
    ?>

    <form action="" method="post">
        <input type='submit' name='atras' value='atrás'/>
        <input type='hidden' name='dir' value='<?= $_SESSION['actualFolder'] ?>'/>
        <label><?= $_SESSION['actualFolder'] ?></label>
        <br>
    </form>
    <br><br>

    <?php
    }

    foreach($_SESSION['scanedDir'] as $item){
        if (is_dir($_SESSION['root'] . '/' . $_SESSION['workingDirectory'].'/'.$item)){ ?>
            <form action="" method="post">
                <input type='submit' name='entrar' value='entrar'/>
                <input type='hidden' name='dir' value='<?= $item ?>'/>
                <label><?= $item ?></label>
                <input type='submit' name='eliminar' value='eliminar'/>
                <input type='submit' name='mover' value='mover'/>
                <br>
            </form>
    <?php }else{ 
            if ($item != "cajon.php"){
        ?>
            <form action="" method="post">
                <input type='submit' name='editar' value='editar'/>
                <input type='hidden' name='file' value='<?= $item ?>'/>
                <a href='<?= $_SESSION['root'] . '/' . $_SESSION['workingDirectory'].'/'.$item ?>' download ><?= $item ?></a>
                <br>
            </form>
    <?php }
        }
    }
    ?>
    <br><br>
    <form action='cajon.php' method='post' enctype="multipart/form-data">

            <input type='submit' name='crearCarpeta' placeholder="nombre de la carpeta" value='crear carpeta'/>
            <input type='text' name='carpeta'/>
            <br>
            
            <input type='file' name='fichero[]' multiple/>
            <br>
            <input type='submit' name='subirFichero' value='subir ficheros'/>
            <br>
            <input type='submit' name='logout' value='logout'/>
            <br>

    </form>
<?php 
} 

if ($_SESSION['editPage'] == true){ ?>
    <br><br>
    <form action='cajon.php' method='post'>
    
            <span>Qué quieres hacer con <?= $_POST['file'] ?>?</span>
            <br>
            <input type='submit' name='cancelar' value='cancelar'/>
            <input type='submit' name='borrar' value='borrar'/>
            <input type='submit' name='renombrar' value='renombrar'/>
            <input type='hidden' name='file' value='<?= $_POST['file'] ?>'/>
            <br>    
    </form>
<?php 
} 

if ($_SESSION['editingPage'] == true){ ?>
    <br><br>
    <form action='cajon.php' method='post'>
    
            <span>Qué nombre quieres darle a <?= $_POST['file'] ?>?</span>
            <br>
            <input type='text' name='newName' value='<?= $_POST['file'] ?>'/>
            <input type='submit' name='cambiar' value='cambiar'/>
            <br>
            <input type='submit' name='cancelar' value='cancelar'/>
            <input type='hidden' name='oldName' value='<?= $_POST['file'] ?>'/>
            <br>    
    </form>
<?php 
} 

if ($_SESSION['login'] == true){ ?>
    <br><br>
    <form action='cajon.php' method='post'>
            <input type='text' name='userName' placeholder="username" />
            <input type='password' name='password' placeholder="password" />
            <input type='submit' name='login' value='login'/>
            <input type='submit' name='register' value='register'/>
            <br>
    </form>


<?php 
}
/*
if ($_SESSION['mover'] == true){ ?>
    <br><br>
    
<?php 

    echo printFoldersTree($_SESSION['root'] . '/' . $_SESSION['user'], $_SESSION['root'] . '/' . $_SESSION['workingDirectory'] . '/' . $_POST['dir']);

?>
    <form action=""method="post">
        <input type='submit' name='volver' value='volver'/>
    </form>
<?php 
} */
?>

</body>
</html>
